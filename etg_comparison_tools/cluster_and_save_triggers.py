#!/usr/bin/env python

# Copyright (C) 2017 Sydney J. Chamberlin, Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# =============================================================================
#
#                               PREAMBLE
#
# =============================================================================

import optparse
import os
import sys
import shutil

from gstlal import idq_aggregator

from etc import triggers

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__date__ = "02/01/2018"
__version__ = "0.1.0"

# =============================================================================
#
#                               FUNCTIONS
#
# =============================================================================

def parse_command_line():
	"""
	Parse command line inputs.
	"""
	parser = optparse.OptionParser()
	parser.add_option("-m", "--trigger-dir", help = "Specify directory containing trigger files. All triggers will be plotted unless GPS time range is specified with --start-time and --stop-time options.")
	parser.add_option("--file-format", default = "ascii", help = "Specify the file format of omicron triggers to be read, default = ascii")
	parser.add_option("--channel", help = "If set, specify the channel of Omicron triggers to process")
	parser.add_option("-o", "--output-dir", action = "store", default = ".", help = "Specify location for output files. Default is current directory.")	
	parser.add_option("-v", "--verbose", action = "store_true", default = False, help = "Be verbose.")
	parser.add_option("-s", "--start-time", type = "int", help = "Plot data for triggers starting at specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("-e", "--end-time", type = "int", help = "Plot data for triggers up to specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("--cluster-cadence", type = "int", default = 1, help = "Sets the cadence at which trigger clustering is done. Default = 1s.")
	parser.add_option("--trigger-type", help = "Specify the type of trigger to cluster, options are 'omicron' or 'gstlal'")

	opts, args = parser.parse_args()
	
	# sanity checks
	if opts.start_time or opts.end_time:
		if not (opts.start_time and opts.end_time):
			parser.error("Need to specify both and a start and an end time.")
	
	return opts


# ===================
#
#        main
#
# ===================

if __name__ == "__main__":
    opts = parse_command_line()

    #
    # read in trigger data + preprocessing
    #
    
    # create instance of idq data class
    if opts.verbose:
    	print >> sys.stderr, "Reading trigger data from file (this will take a moment)..."
    
    if opts.trigger_type == 'gstlal':
        trgdata = triggers.GstlalIdqTriggerData(opts.trigger_dir, start_time = opts.start_time, end_time = opts.end_time, cluster_cadence = opts.cluster_cadence, file_format = opts.file_format)
    elif opts.trigger_type == 'omicron':
       	trgdata = triggers.OmicronTriggerData(opts.trigger_dir, start_time = opts.start_time, end_time = opts.end_time, cluster_cadence = opts.cluster_cadence, file_format = opts.file_format, channel = opts.channel)
    else:
        raise ValueError, 'not a valid trigger type'

    if opts.verbose:
    	print >> sys.stderr, "...done.\n"
    
    #print >>sys.stderr, "all channels: %s" % trgdata.keys()
    
    # save triggers to disk for future processing
    span = '%d_%d' % (opts.start_time, opts.end_time - opts.start_time)
    path_base = 'comparison_triggers-%s' % span

    for channel in trgdata.keys():
        idq_aggregator.create_new_dataset(opts.output_dir, path_base, trgdata[channel], name = span, group = channel, tmp = True)

    # finish saving
    final_path = os.path.join(opts.output_dir, path_base) + '.h5'
    tmp_path = final_path + '.tmp'

    shutil.move(tmp_path, final_path)
