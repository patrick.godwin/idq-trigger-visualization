# Copyright (C) 2017 Sydney J. Chamberlin, Patrick Godwin, Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# =============================================================================
#
#                               PREAMBLE
#
# =============================================================================

from collections import defaultdict 
from collections import namedtuple
import datetime
import itertools
import glob
import math
import optparse
import os
import sys

import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.colorbar import Colorbar
from matplotlib import pyplot as plt
from matplotlib import ticker as tkr
import matplotlib.cm as cm
import numpy

from lal import GPSToUTC

__author__ = "Sydney J. Chamberlin (sydney.chamberlin@ligo.org)"
__date__ = "07/17/2017"
__version__ = "2.1.1"

matplotlib.rcParams.update({
        "font.size": 10.0,
        "axes.titlesize": 10.0,
        "axes.labelsize": 10.0,
        "xtick.labelsize": 8.0,
        "ytick.labelsize": 8.0,
        "legend.fontsize": 8.0,
        "figure.dpi": 200,
        "savefig.dpi": 200
})

color_scheme = ['#332288', '#88CCEE', '#44AA99', '#117733', '#999933', '#DDCC77', '#CC6677', '#882255', '#AA4499']

# =============================================================================
#
#                                CLASSES
#
# =============================================================================

class GstlalIdqTriggerData(object):

        def __init__(self, triggerdir):
                """
                Returns object with various gstlal_idq_trigger_gen trigger parameters.
                """
		self.triggerdir = triggerdir
		
		# if specific gps time is set, grab only relevant triggers
		if opts.start_time and opts.end_time:
			if opts.plot_cadence:
				self.gps_ranges = ['%d_%d' % (gps_time, min(gps_time + opts.plot_cadence, opts.end_time)) for gps_time in range(opts.start_time, opts.end_time, opts.plot_cadence)]
			else:
				self.gps_ranges = ['%d_%d' % (opts.start_time, opts.end_time)]

		else:
			self.gps_ranges = None

		# grab relevant trigger files for each gps range
		if self.gps_ranges is not None:
			glob_exps = {}
			for gps_range in self.gps_ranges:
				interval = opts.end_time - opts.start_time
				gps_idx = len(str(opts.end_time)) - len(str(interval))
				glob_exps[gps_range] = '*'
				for idx in range(len(str(opts.end_time))):
					if idx == gps_idx:
						effective_start_time = (opts.start_time / opts.trigger_cadence) * opts.trigger_cadence
						glob_exps[gps_range] += '[' + str(effective_start_time)[idx] + '-' + str(opts.end_time - 1)[idx] + ']'
					else:
						glob_exps[gps_range] += '[0-9]'
				glob_exps[gps_range] += '*.trg'
		else:
			glob_exps = '*.trg'

		# create paths if they don't exist
		if self.gps_ranges is not None:
			for gps_range in self.gps_ranges:
				path = os.path.join(opts.output_dir, gps_range)
				if not os.path.exists(path):
					try:
						os.makedirs(path)
					except IOError:
						pass
					except OSError:
						pass

		# extract trigger data and match column headings to data 
		if self.gps_ranges is not None:
			trgfiles = {gps_range: glob.glob(os.path.join(self.triggerdir, glob_exps[gps_range])) for gps_range in self.gps_ranges}
			triggers = {gps_range: itertools.chain.from_iterable(open(trgfile, 'r') for trgfile in trgfiles[gps_range]) for gps_range in self.gps_ranges}
		else:
			trgfiles = glob.glob(os.path.join(self.triggerdir, glob_exps))
			triggers = itertools.chain.from_iterable(open(trgfile, 'r') for trgfile in trgfiles)

		#FIXME: if we use dtype = None, length of diff chnl names means this may break, but should 
		#       eliminate fixed dtype somehow, or extract elsewhere in case format of file changes in future
		#self.tdata = numpy.genfromtxt(triggers, dtype=None, names=True)
		trg_dtype = [('start_time', '<f8'), ('stop_time', '<f8'), ('trigger_time', '<f8'), ('frequency', '<f8'), 
		             ('phase', '<f8'), ('sigmasq', '<f8'), ('chisq', '<f8'), ('snr', '<f8'), ('channel', 'S80')]
		#trg_dtype = [('start_time', '<f8'), ('stop_time', '<f8'), ('trigger_time', '<f8'), ('frequency', '<f8'), 
		#              ('phase', '<f8'), ('sigmasq', '<f8'), ('chisq', '<f8'), ('snr', '<f8'), ('channel', 'S46'), ('latency', '<f8'), ('Q', '<f8')]

		if self.gps_ranges is not None:
			self.tdata = {gps_range: numpy.genfromtxt(triggers[gps_range], names=True, dtype=(trg_dtype)) for gps_range in self.gps_ranges}
		else:
			self.tdata = numpy.genfromtxt(triggers, names=True, dtype=(trg_dtype))
		
		# how much GPS time was just read in?
		if self.gps_ranges is not None:
			self.gps_sec_elapsed = opts.plot_cadence
		else:
			self.gps_sec_elapsed = opts.trigger_cadence * len(trgfiles)

		# Define named structures for clarity
		Row = namedtuple('Row', ['trigger_time', 'snr'])
		if self.gps_ranges is not None:
			Channel = namedtuple('Channel', ['gps_range', 'name', 'rate'])
		else:
			Channel = namedtuple('Channel', ['name', 'rate'])

		# construct trigger dict keyed by channel name, rate
		self.channel_grps = set()
		self.triggerdict = {}
		if self.gps_ranges is not None:
			for gps_range in self.gps_ranges:
				for row in self.tdata[gps_range]:
					# FIXME: for some reason, calling by name breaks for very long
					#        files. could be because there's no header on them?
					ch_name, _, high_freq = row[8].rsplit('_', 2)
					ifo_grp, _ = ch_name.split('-', 1)
					ifo, channel_grp = ifo_grp.split('_', 1)
					self.channel_grps.add(channel_grp)
					#ch_name, _, high_freq = row['channel'].rsplit('_', 2)
					rate = int(high_freq) * 2
					ch_key = Channel(gps_range = gps_range, name = ch_name, rate = rate)
					if ch_key not in self.triggerdict:
						self.triggerdict[ch_key] = []
					gps_start, gps_end = gps_range.split('_')
					# filter out rows that don't span the gps range
					#if row['trigger_time'] >= int(gps_start) and row['trigger_time'] <= int(gps_end) + 1:
					if row[2] >= int(gps_start) and row[2] <= int(gps_end) + 1:
						#trigger_row = Row(trigger_time = row['trigger_time'], snr = row['snr'])
						trigger_row = Row(trigger_time = row[2], snr = row[7])
						self.triggerdict[ch_key].append(trigger_row)
		else:
			for row in self.tdata:
				ch_name, _, high_freq = row['channel'].rsplit('_', 2)
				rate = int(high_freq) * 2
				ch_key = Channel(name = ch_name, rate = rate)
				if ch_key not in self.triggerdict:
					self.triggerdict[ch_key] = []
				trigger_row = Row(trigger_time = row['trigger_time'], snr = row['snr'])
				self.triggerdict[ch_key].append(trigger_row)

		# channel names and unique rates
		self.ch_names = set(channel.name for channel in self.triggerdict.iterkeys())
		self.ch_rates = set(channel.rate for channel in self.triggerdict.iterkeys())

		# find trigger times spanned from trigger files
		if self.gps_ranges is None:
			times = [row[0] for triggers in self.triggerdict.values() for row in triggers]
			self.min_time, self.max_time = (min(times), max(times))
		else:
			self.min_time, self.max_time = (opts.start_time, opts.end_time)

class OmicronTriggerData(object):

        def __init__(self, triggerdir):
                """
                Returns object with various gstlal_idq_trigger_gen trigger parameters.
                """
		self.triggerdir = triggerdir
		self.gps_ranges = None
		self.ch_name = opts.omicron_channel
	
		# if specific gps time is set, grab only relevant triggers
		if opts.start_time and opts.end_time:
			if opts.plot_cadence:
				self.gps_ranges = ['%d_%d' % (gps_time, min(gps_time + opts.plot_cadence, opts.end_time)) for gps_time in range(opts.start_time, opts.end_time, opts.plot_cadence)]
			else:
				self.gps_ranges = ['%d_%d' % (opts.start_time, opts.end_time)]

		else:
			self.gps_ranges = None

		# grab relevant trigger files for each gps range
		if self.gps_ranges is not None:
			glob_exps = {}
			for gps_range in self.gps_ranges:
				interval = opts.end_time - opts.start_time
				gps_idx = len(str(opts.end_time)) - len(str(interval))
				glob_exps[gps_range] = '*/%s/*' % self.ch_name
				for idx in range(len(str(opts.end_time))):
					if idx == gps_idx:
						effective_start_time = (opts.start_time / opts.trigger_cadence) * opts.trigger_cadence
						glob_exps[gps_range] += '[' + str(effective_start_time)[idx] + '-' + str(opts.end_time - 1)[idx] + ']'
					else:
						glob_exps[gps_range] += '[0-9]'
				glob_exps[gps_range] += '*.txt'
		else:
			glob_exps = '*/%s/*.txt' % self.ch_name

		# create paths if they don't exist
		if self.gps_ranges is not None:
			for gps_range in self.gps_ranges:
				path = os.path.join(opts.output_dir, gps_range)
				if not os.path.exists(path):
					try:
						os.makedirs(path)
					except IOError:
						pass
					except OSError:
						pass

		# extract trigger data and match column headings to data 
		if self.gps_ranges is not None:
			trgfiles = {gps_range: glob.glob(os.path.join(self.triggerdir, glob_exps[gps_range])) for gps_range in self.gps_ranges}
			triggers = {gps_range: [line for trgfile in trgfiles[gps_range] for line in open(trgfile, 'r') if line[0] != "#"] for gps_range in self.gps_ranges}
		else:
			trgfiles = glob.glob(os.path.join(self.triggerdir, glob_exps))
			triggers = [line for trgfile in trgfiles for line in open(trgfile, 'r') if line[0] != "#"]

		#FIXME: if we use dtype = None, length of diff chnl names means this may break, but should 
		#       eliminate fixed dtype somehow, or extract elsewhere in case format of file changes in future
		#self.tdata = numpy.genfromtxt(triggers, dtype=None, names=True)
		trg_dtype = [('central_time', '<f8'), ('central_f', '<f8'), ('snr', '<f8'), ('q', '<f8'), 
		             ('amplitude', '<f8'), ('phase', '<f8'), ('start_time', '<f8'), ('end_time', '<f8'), ('start_f', '<f8'), ('end_f', '<f8')]

		if self.gps_ranges is not None:
			self.tdata = {gps_range: numpy.genfromtxt(triggers[gps_range], names=True, dtype=(trg_dtype)) for gps_range in self.gps_ranges}
		else:
			self.tdata = numpy.genfromtxt(triggers, names=True, dtype=(trg_dtype))
		
		# how much GPS time was just read in?
		if self.gps_ranges is not None:
			self.gps_sec_elapsed = opts.plot_cadence
		else:
			self.gps_sec_elapsed = opts.trigger_cadence * len(trgfiles)

		# Define named structures for clarity
		Row = namedtuple('Row', ['trigger_time', 'snr'])
		if self.gps_ranges is not None:
			Channel = namedtuple('Channel', ['gps_range', 'name', 'rate'])
		else:
			Channel = namedtuple('Channel', ['name', 'rate'])

		# construct trigger dict keyed by channel name, rate
		self.channel_grps = set()
		self.triggerdict = {}
		if self.gps_ranges is not None:
			for gps_range in self.gps_ranges:
				for row in self.tdata[gps_range]:
					rate = 2 ** (math.ceil(math.log(row[1])/math.log(2)) + 1)
					ch_key = Channel(gps_range = gps_range, name = self.ch_name, rate = rate)
					if ch_key not in self.triggerdict:
						self.triggerdict[ch_key] = []
					gps_start, gps_end = gps_range.split('_')
					# filter out rows that don't span the gps range
					if row[0] >= int(gps_start) and row[0] <= int(gps_end) + 1:
						trigger_row = Row(trigger_time = row[0], snr = row[2])
						self.triggerdict[ch_key].append(trigger_row)
		else:
			for row in self.tdata:
				rate = 2 ** (math.ceil(math.log(row[1])/math.log(2)) + 1)
				ch_key = Channel(name = self.ch_name, rate = rate)
				if ch_key not in self.triggerdict:
					self.triggerdict[ch_key] = []
				trigger_row = Row(trigger_time = row[0], snr = row[2])
				self.triggerdict[ch_key].append(trigger_row)

		# channel names and unique rates
		self.ch_names = set(channel.name for channel in self.triggerdict.iterkeys())
		self.ch_rates = set(channel.rate for channel in self.triggerdict.iterkeys())

		# find trigger times spanned from trigger files
		if self.gps_ranges is None:
			times = [row[0] for triggers in self.triggerdict.values() for row in triggers]
			self.min_time, self.max_time = (min(times), max(times))
		else:
			self.min_time, self.max_time = (opts.start_time, opts.end_time)

# =============================================================================
#
#                               FUNCTIONS
#
# =============================================================================

def parse_command_line():
	"""
	Parse command line inputs.
	"""
	parser = optparse.OptionParser()
	parser.add_option("-d", "--trigger-dir", help = "Specify directory containing trigger files. All triggers will be plotted unless GPS time range is specified with --start-time and --stop-time options.")
	parser.add_option("-o", "--output-dir", action = "store", default = ".", help = "Specify location for output files. Default is current directory.")	
	parser.add_option("-a", "--plot-all", action = "store_true", help = "Create all possible plots.")
	parser.add_option("-n", "--snr-EM", action = "store_true", help = "Create eventmap displaying SNR values for each channel sample rate.")
	parser.add_option("-r", "--sngl-rate-EM", action = "store_true", help = "Create eventmap for a single sample rate and each data channel.")
	parser.add_option("-c", "--sngl-chnl-EM", action = "store_true",help = "Create eventmap for a single channel and each sample rate.")
	parser.add_option("-p", "--snr-hist", action = "store_true", help = "Create histogram of SNR values.")
	parser.add_option("-t", "--snr-time", action = "store_true", help = "Plot SNR vs. time for each data channel.")
	parser.add_option("-l", "--latency", action = "store_true", help = "Plot latency vs. time for each data channel.")
	parser.add_option("-b", "--latency-by-rate", action = "store_true", help = "Plot latency vs. time by sample rate, for each data channel.")
	parser.add_option("-v", "--verbose", action = "store_true", default = False, help = "Be verbose.")
	parser.add_option("-s", "--start-time", type = "int", help = "Plot data for triggers starting at specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("-e", "--end-time", type = "int", help = "Plot data for triggers up to specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("--trigger-cadence", type = "int", default = 32, help = "Sets the cadence at which triggers are produced from gstlal-idq pipeline. Default = 32s.") 
	parser.add_option("--plot-cadence", type = "int", help = "Sets the cadence at which plots are produced. If not selected, will make one set of plots spanning the entire gps range.") 
	parser.add_option("--omicron", action = "store_true", default = False, help = "Use omicron triggers to plot instead of gstlal-iDQ triggers. Default = False.")
	parser.add_option("--omicron-channel", help = "If using Omicron triggers, specify the channel used.")

	opts, args = parser.parse_args()
	
	# sanity checks
	if opts.trigger_dir is None:
		parser.error("--trigger-dir must be specified to indicate location of trigger data.")
	if not opts.omicron:
		if not glob.glob(os.path.join(opts.trigger_dir, '*.trg')):
			parser.error("No *.trg files found in --trigger-dir. At least one file containing triggers must be specified using --trigger-dir.")
	if opts.start_time or opts.end_time:
		if not (opts.start_time and opts.end_time):
			parser.error("Need to specify both and a start and an end time.")
		if opts.plot_cadence:
			if opts.plot_cadence > (opts.end_time - opts.start_time):
				parser.error("Plot cadence can't be longer than the gps range specified.")
	# for verbose:
	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "Getting to work... "
	
	return opts

def determine_time_scale(tmin, tmax):
	time_int = tmax-tmin
	nticks = 6
	#FIXME: make this scaling more robust, this is kind of hackish
	# use hours
	if time_int > 6400.:
		hourtime = time_int/3600.
		stepsize = numpy.ceil(hourtime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*3600.)
		ticklabelrange = numpy.arange(0, hourtime, stepsize) 
		axlabel = 'hours'

	# use minutes
	elif time_int < 6400.:
		minutetime = time_int/60.
		stepsize = numpy.ceil(minutetime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*60.)
		ticklabelrange = numpy.arange(0, minutetime, stepsize)
		axlabel = 'minutes'	

	return tickrange, ticklabelrange, axlabel

def sngl_chnl_eventmap(idqobject, channelname, gps_range = None):
	"""
	Produces event plot for a single channel, sorted by sample rates and colored by SNR.
	"""
	fig, axes = plt.subplots()

	trgdict = idqobject.triggerdict

	if gps_range is not None:
		rate_keys = sorted([channel for channel in trgdict if channel.name == channelname and channel.gps_range == gps_range], key = lambda channel: channel.rate)
	else:
		rate_keys = sorted([channel for channel in trgdict if channel.name == channelname], key = lambda channel: channel.rate)

	if opts.verbose:
		print >> sys.stderr, "Producing single channel eventmap for channel %s..." % str(channelname)

	# pyplot.vlines only accepts rgba colors, so map SNR values to a colormap
	# produces a list of rgba values for each channel
	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "Mapping trigger SNR values to RGBA tuples..."
	
	snrs = [row.snr for key in rate_keys for row in trgdict[key]]
	if not snrs:
		raise RuntimeError("No data found for channel %s for this gps range" % channel)

	snr_min, snr_max = min(snrs), max(snrs)

	snr_norm = matplotlib.colors.Normalize(vmin=snr_min, vmax=snr_max, clip=True)
	mapper = cm.ScalarMappable(norm = snr_norm, cmap = cm.copper)
	rgba_list = [mapper.to_rgba(snrval) for snrval in snrs]
	
	yticks = []
	for idx, key in enumerate(rate_keys):
		trgtimes = [row.trigger_time for row in trgdict[key]]
		# some channels have no data for some rates
		if trgtimes:
			yticks.append(idx+0.5)
			cax = axes.vlines(trgtimes, idx, idx+1, colors = rgba_list)
	
	axes.set_title(r"Eventmap for channel %s" % str(channelname))

	# y axis
	ylabels = [str(channel.rate) for channel in rate_keys]
	axes.set_yticks(yticks)
	axes.set_yticklabels(ylabels)
	axes.set_ylabel(r"Sample Rate [Hz]") 

	if gps_range is not None:
		gps_start, gps_end = gps_range.split('_')
		gps_start, gps_end = (int(gps_start), int(gps_end))
	else:
		gps_start, gps_end = (idqobject.min_time, idqobject.max_time)

	# x axis
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	
	#print >> sys.stderr, xticks, xticklabelmarks, channel

	xticklabelvals = [str(i) for i in xticklabelmarks]
	axes.set_xlim(gps_start, gps_end)
	axes.xaxis.set_ticks(xticks)
	axes.set_xticklabels(xticklabelvals)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel, datetimefmt))
	
	# set up colorbar
	divider = make_axes_locatable(axes)
	cax = divider.append_axes( "right", size="5%", pad=0.1)
	cbl = matplotlib.colorbar.ColorbarBase(cax, cmap = matplotlib.cm.copper, norm = snr_norm, orientation="vertical")
	cbl.set_label(r"Signal-to-noise ratio")

	plt.tight_layout()

	return fig, axes

def sngl_rate_eventmap(idqobject, samprate, gps_range = None):
	"""
	Produces event plot for a given sample rate and several data channels; lines are colored by SNR.
	"""
	fig = plt.figure()
	axes = fig.add_subplot(111)

	trgdict = idqobject.triggerdict
	if gps_range is not None:
		channel_keys = [channel for channel in trgdict if channel.rate == samprate and channel.gps_range == gps_range]
	else:
		channel_keys = [channel for channel in trgdict if channel.rate == samprate]
	
	if opts.verbose:
		print >> sys.stderr, "Producing multi-channel eventmap for sample rate %s Hz..." % str(samprate)

	# pyplot.vlines only accepts rgba colors, so map SNR values to a colormap
	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "mapping trigger SNR values to RGBA tuples..."
	
	# find min and max snr contained in all the channels and use to establish colormap
	"""
	Here we want the colors to apply equally to each row of channels.
	"""
	snrs = [row.snr for key in channel_keys for row in trgdict[key]]
	if not snrs:
		raise RuntimeError("No data found for sampling rate %d for this gps range" % samprate)

	snr_min, snr_max = min(snrs), max(snrs)

	snr_norm = matplotlib.colors.Normalize(vmin=snr_min, vmax=snr_max, clip=True)
	mapper = cm.ScalarMappable(norm = snr_norm, cmap = cm.hot)

	# generate vline plot
	yticks = []
	for idx, key in enumerate(channel_keys):
		if trgdict[key]:
			yticks.append(idx+0.5)
			trgtimes = [row.trigger_time for row in trgdict[key]]
			chsnrs = [row.snr for row in trgdict[key]]
			rgba_list = [mapper.to_rgba(snrval) for snrval in chsnrs]
			#FIXME: remove whitespace that occurs when a given channel has no data at this sample rate
			axes.vlines(trgtimes, idx, idx + 1, colors = rgba_list)

	axes.set_title(r"Eventmap for sample rate %s Hz" % str(samprate))

	# y axis
	ylabels = [key.name for key in channel_keys]
	axes.set_yticks(yticks)
	axes.set_yticklabels(ylabels)
	axes.set_ylabel(r"Data Channels")

	if gps_range is not None:
		gps_start, gps_end = gps_range.split('_')
		gps_start, gps_end = (int(gps_start), int(gps_end))
	else:
		gps_start, gps_end = (idqobject.min_time, idqobject.max_time)

	# x axis
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	xticklabelvals = [str(i) for i in xticklabelmarks]
	axes.set_xlim(gps_start, gps_end)
	axes.xaxis.set_ticks(xticks)
	axes.set_xticklabels(xticklabelvals)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))

	# set up colorbar
	divider = make_axes_locatable(axes)
	cax = divider.append_axes( "right", size="5%", pad=0.1)
	cbl = matplotlib.colorbar.ColorbarBase(cax, cmap = matplotlib.cm.hot, norm = snr_norm, orientation="vertical")
	cbl.set_label(r"Signal-to-noise ratio")

	plt.tight_layout()

	return fig, axes

def snr_eventmap(idqobject, channelname, gps_range = None):
	"""
	Produces SNR eventmap based on sample rate for a single channel. 
	"""
	fig, axes = plt.subplots()

        # match channelname to appropriate dataset
        trgdict = idqobject.triggerdict

	if gps_range is not None:
		rate_keys = sorted([channel for channel in trgdict if channel.name == channelname and channel.gps_range == gps_range], key = lambda channel: channel.rate)
	else:
		rate_keys = sorted([channel for channel in trgdict if channel.name == channelname], key = lambda channel: channel.rate)

	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "Creating SNR event map for %s:" % channelname

	yticks = []
	for rdx, key in enumerate(rate_keys):
		if trgdict[key]:
			yticks.append(rdx+0.5)
			snrs = [row.snr for row in trgdict[key]]
			if not snrs:
				raise RuntimeError("No data found for channel %s for this gps range" % channelname)
			cax = axes.vlines(snrs, rdx, rdx+1)

	labels = [str(key.rate) for key in rate_keys]
	axes.set_title(r"SNR vs. sample rate for channel %s" % str(channelname))
	axes.set_yticks(yticks)
	axes.set_yticklabels(labels)
	axes.set_xlabel(r"Signal-to-noise ratio (SNR)")
	axes.set_ylabel(r"Sample Rate [Hz]") 

	plt.tight_layout()

	return fig, axes

def snr_hist(idqobject, channelname, gps_range = None):
	fig = plt.figure()
	axes = fig.add_subplot(111)

	trgdict = idqobject.triggerdict

	if gps_range is not None:
		rate_keys = sorted([channel for channel in trgdict if channel.name == channelname and channel.gps_range == gps_range], key = lambda channel: channel.rate)
	else:
		rate_keys = sorted([channel for channel in trgdict if channel.name == channelname], key = lambda channel: channel.rate)
	nbins = 20

	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "Producing SNR histogram for channel %s..." % channelname
	
	chnl_rts = [key.rate for key in rate_keys]
	chnl_rt_snrs = [[row.snr for row in trgdict[key]] for key in rate_keys if trgdict[key]]
	if not chnl_rt_snrs:
		raise RuntimeError("No data found for channel %s for this gps range" % channelname)

	axes.hist(chnl_rt_snrs, nbins, normed = 1, log = True, stacked = True, histtype = 'barstacked', rwidth = 1.0, 
	          color = color_scheme[0:len(chnl_rts)], edgecolor = "none", label = [str(rt) + ' Hz' for rt in chnl_rts])
	
	axes.set_title(r"Trigger SNRs for %s" % channelname)
	axes.set_ylabel(r"Probability density")
	axes.set_xlabel(r"Signal to noise ratio (SNR)") 
	axes.legend()

	return fig, axes

def snr_vs_time(idqobject, gps_range = None):
	fig = plt.figure()
	axes = fig.add_subplot(111) 

	trgdict = idqobject.triggerdict

	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "Creating ln(snr) versus time figure:"

	for idx, channel_grp in enumerate(idqobject.channel_grps):
		for channelname in idqobject.ch_names:
			if gps_range is not None:
				keys = [channel for channel in trgdict if (channel.name == channelname and channel_grp in channelname and channel.gps_range == gps_range)]
			else:
				keys = [channel for channel in trgdict if channel.name == channelname and channel_grp in channelname]
			print >> sys.stderr, "... adding data for %s" % channelname
			snr_time_list = [(row.snr, row.trigger_time) for key in keys for row in trgdict[key] if (trgdict[key] and row.snr != 0)]
			if snr_time_list:
				snrs, trg_times = zip(*snr_time_list)
				axes.scatter(trg_times, numpy.log(snrs), color = color_scheme[idx % 9], alpha = 0.3, s = 3)
	
	if gps_range is not None:
		gps_start, gps_end = gps_range.split('_')
		gps_start, gps_end = (int(gps_start), int(gps_end))
	else:
		gps_start, gps_end = (idqobject.min_time, idqobject.max_time)

	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	axes.set_title(r"ln(SNR) vs. trigger time")
	axes.set_ylabel(r"ln(SNR)")
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	lgd = axes.legend(idqobject.ch_names, bbox_to_anchor=(1, 1))

	# to keep the legend from getting chopped off
	#handles, labels = axes.get_legend_handles_labels()
	#lgd = axes.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5,-0.1))
	#axes.grid('on')
	#fig.savefig('samplefigure', bbox_extra_artists=(lgd,), bbox_inches='tight')

	return fig, axes, lgd

def latency_vs_time(idqobject, gps_range = None):
        fig = plt.figure()
        axes = fig.add_subplot(111)

	trgdict = idqobject.triggerdict

	if opts.verbose:
		print >> sys.stderr, ""
        	print >> sys.stderr, "Creating latency versus time figure..."

        for channel in idqobject.ch_names:
		if gps_range is not None:
			keys = [channel for channel in trgdict if channel.name == channelname and channel.gps_range == gps_range]
		else:
			keys = [channel for channel in trgdict if channel.name == channelname]
		trg_times = [row.trigger_time for row in trgdict[key] for key in keys]
		latencies = [row.latency for row in trgdict[key] for key in keys]
		axes.scatter(trg_times, latencies)

	if gps_range is not None:
		gps_start, gps_end = gps_range.split('_')
		gps_start, gps_end = (int(gps_start), int(gps_end))
	else:
		gps_start, gps_end = (idqobject.min_time, idqobject.max_time)

        xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
        datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
        axes.set_title(r'Latency vs. trigger time')
        #axes.set_xlim(1.17984642e+09,1.17984678e+09)
        axes.set_ylabel(r"Latency")
        axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
        axes.set_xticks(xticks)
        axes.set_xticklabels(xticklabelmarks)

        return fig, axes

def latency_vs_time_by_rate(idqobject, samprate):
	fig = plt.figure()
	axes = fig.add_subplot(111)

	if opts.verbose:
		print >> sys.stderr, ""
		print >> sys.stderr, "Creating latency versus time figure for %s..." % str(rate)
	
	trgdict = idqobject.triggerdict
        for chnl in idqobject.data_channels:
		axes.scatter(trgdict[(chnl, samprate)].snr, trgdict[(chnl, samprate)].latency)	

	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(idqobject.min_time, idqobject.max_time)	
	datetimefmt = datetime.datetime(*GPSToUTC(int(idqobject.min_time))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(idqobject.min_time).split('.')[0]+")"
	axes.set_title(r'Latency vs. trigger time')
	axes.set_ylabel(r"Latency in %s" % channel)
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)
	lgd = axes.legend(idqobject.data_channels, bbox_to_anchor=(1, 1))

	return fig, axes, lgd


#
# =============================================================================
#
#                              Generate Plots
#
# =============================================================================
#

# create idq data object from file path
opts = parse_command_line()

# create instance of idq data class
if opts.verbose:
	print >> sys.stderr, ""
	print >> sys.stderr, "Reading trigger data from file (this will take a moment)..."

if opts.omicron:
	trgdata = OmicronTriggerData(opts.trigger_dir)
else:
	trgdata = GstlalIdqTriggerData(opts.trigger_dir)

if opts.verbose:
	print >> sys.stderr, "...done."

# plots that loop over channels
for chnlname in trgdata.ch_names:
	# create single channel event maps
	if opts.sngl_chnl_EM or opts.plot_all:
		if trgdata.gps_ranges is not None:
			for gps_range in trgdata.gps_ranges:
				try:
					emfig = sngl_chnl_eventmap(trgdata,chnlname, gps_range = gps_range)[0]
					fname = 'snglchnleventmap_%s.png' % chnlname
					emfig.savefig(os.path.join(os.path.join(opts.output_dir, gps_range), fname))
					plt.close(emfig)
				except RuntimeError:
					print >>sys.stderr, "No valid data available for this plot, skipping..."
		else:
			try:
				emfig = sngl_chnl_eventmap(trgdata,chnlname)[0]
				fname = 'snglchnleventmap_%s.png' % chnlname
				emfig.savefig(os.path.join(opts.output_dir, fname))
				plt.close(emfig)
			except RuntimeError:
				print >>sys.stderr, "No valid data available for this plot, skipping..."

	# create SNR histograms
	if opts.snr_hist or opts.plot_all:
		if trgdata.gps_ranges is not None:
			for gps_range in trgdata.gps_ranges:
				try:
					hist_fig = snr_hist(trgdata,chnlname, gps_range = gps_range)[0]
					hist_fname = 'snrhist_%s.png' % chnlname
					hist_fig.savefig(os.path.join(os.path.join(opts.output_dir, gps_range), hist_fname))
					plt.close(hist_fig)
				except RuntimeError:
					print >>sys.stderr, "No valid data available for this plot, skipping..."
		else:
			try:
				hist_fig = snr_hist(trgdata,chnlname)[0]
				hist_fname = 'snrhist_%s.png' % chnlname
				hist_fig.savefig(os.path.join(opts.output_dir, hist_fname))
				plt.close(hist_fig)
			except RuntimeError:
				print >>sys.stderr, "No valid data available for this plot, skipping..."

	# create SNR event maps
	if opts.snr_EM or opts.plot_all:
		if trgdata.gps_ranges is not None:
			for gps_range in trgdata.gps_ranges:
				try:
					emfig = snr_eventmap(trgdata,chnlname, gps_range)[0]
					fname = 'snreventmap_%s.png' % chnlname
					emfig.savefig(os.path.join(os.path.join(opts.output_dir, gps_range), fname))
					plt.close(emfig)
				except RuntimeError:
					print >>sys.stderr, "No valid data available for this plot, skipping..."
		else:
			try:
				emfig = snr_eventmap(trgdata,chnlname)[0]
				fname = 'snreventmap_%s.png' % chnlname
				emfig.savefig(os.path.join(opts.output_dir, fname))
				plt.close(emfig)
			except RuntimeError:
				print >>sys.stderr, "No valid data available for this plot, skipping..."

#if opts.latency or opts.plot_all:
#	latfig = latency_vs_time(trgdata)[0]
#	fname = 'latency'+'_'.join(chnlname)+'.png'
#	latfig.savefig(fname)
#	plt.close(latfig)

# create single rate event map
for rate in trgdata.ch_rates:
	if opts.sngl_rate_EM or opts.plot_all:
		if trgdata.gps_ranges is not None:
			for gps_range in trgdata.gps_ranges:
				try:
					emfig = sngl_rate_eventmap(trgdata,rate, gps_range = gps_range)[0]
					fname = 'snglrateeventmap_%dHz.png' % rate
					emfig.savefig(os.path.join(os.path.join(opts.output_dir, gps_range), fname))
					plt.close(emfig)
				except RuntimeError:
					print >>sys.stderr, "No valid data available for this plot, skipping..."
		else:
			try:
				emfig = sngl_rate_eventmap(trgdata,rate)[0]
				fname = 'snglrateeventmap_%dHz.png' % rate
				emfig.savefig(os.path.join(opts.output_dir, fname))
				plt.close(emfig)
			except RuntimeError:
				print >>sys.stderr, "No valid data available for this plot, skipping..."

#	if opts.latency_by_rate or opts.plot_all:
#		latfix, latax, lgd = latency_vs_time_by_rate(trgdata,rate)
#		fname = str(outdir)+'latency_vs_time_'+str(rate)+'Hz.png'
#		latfix.savefig(fname, bbox_extra_artists=(lgd,), bbox_inches='tight')
#		plt.close(emfig)

# snr vs time 
if opts.snr_time or opts.plot_all:
	if trgdata.gps_ranges is not None:
		for gps_range in trgdata.gps_ranges:
			try:
				snrfig, snrax, lgd = snr_vs_time(trgdata, gps_range = gps_range)
				fname = 'snrvstime.png'
				snrfig.savefig(os.path.join(os.path.join(opts.output_dir, gps_range), fname), bbox_extra_artists=(lgd,), bbox_inches='tight')
				plt.close(snrfig)
			except RuntimeError:
				print >>sys.stderr, "No valid data available for this plot, skipping..."
	else:
		try:
			snrfig, snrax, lgd = snr_vs_time(trgdata)
			fname = 'snrvstime.png'
			snrfig.savefig(os.path.join(opts.output_dir, fname), bbox_extra_artists=(lgd,), bbox_inches='tight')
			plt.close(snrfig)
		except RuntimeError:
			print >>sys.stderr, "No valid data available for this plot, skipping..."
