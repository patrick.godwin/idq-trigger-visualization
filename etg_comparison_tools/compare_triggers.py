#!/usr/bin/env python

# Copyright (C) 2017 Sydney J. Chamberlin, Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# =============================================================================
#
#                               PREAMBLE
#
# =============================================================================

from collections import defaultdict 
from collections import deque
from collections import namedtuple
import datetime
import itertools
import glob
import math
import string
import optparse
import os
import sys
import urlparse

import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.colorbar import Colorbar
from matplotlib import pyplot as plt
from matplotlib import ticker as tkr
from decimal import Decimal
import matplotlib.cm as cm
import numpy
from textwrap import wrap

from lal import GPSToUTC
from glue import markup

from etc import triggers
from etc import utils

__author__ = "Sydney J. Chamberlin (sydney.chamberlin@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__date__ = "07/17/2017"
__version__ = "2.1.1"

matplotlib.rcParams.update({
        "font.size": 10.0,
        "axes.titlesize": 10.0,
        "axes.labelsize": 10.0,
        "xtick.labelsize": 8.0,
        "ytick.labelsize": 8.0,
        "legend.fontsize": 8.0,
        "figure.dpi": 200,
        "savefig.dpi": 200
})

cluster_urls = {'CIT': 'https://ldas-jobs.ligo.caltech.edu/',
                'LHO': 'https://ldas-jobs.ligo-wa.caltech.edu/',
                'LLO': 'https://ldas-jobs.ligo-la.caltech.edu/',
                'uwm': 'https://ldas-jobs.cgca.uwm.edu/'
               }

color_scheme = ['#332288', '#88CCEE', '#44AA99', '#117733', '#999933', '#DDCC77', '#CC6677', '#882255', '#AA4499']
colors = ['#2c7fb8', '#e66101', '#5e3c99', '#d01c8b']

# =============================================================================
#
#                               FUNCTIONS
#
# =============================================================================

def parse_command_line():
	"""
	Parse command line inputs.
	"""
	parser = optparse.OptionParser()
	parser.add_option("-g", "--gstlal-trigger-dir", help = "Specify directory containing trigger files. All triggers will be plotted unless GPS time range is specified with --start-time and --stop-time options.")
	parser.add_option("-m", "--omicron-trigger-dir", help = "Specify directory containing trigger files. All triggers will be plotted unless GPS time range is specified with --start-time and --stop-time options.")
	parser.add_option("--gstlal-file-format", default = "ascii", help = "Specify the file format of gstlal triggers to be read, default = ascii")
	parser.add_option("--omicron-file-format", default = "ascii", help = "Specify the file format of omicron triggers to be read, default = ascii")
	parser.add_option("-o", "--output-dir", action = "store", default = ".", help = "Specify location for output files. Default is current directory.")	
	parser.add_option("-v", "--verbose", action = "store_true", default = False, help = "Be verbose.")
	parser.add_option("-s", "--start-time", type = "int", help = "Plot data for triggers starting at specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("-e", "--end-time", type = "int", help = "Plot data for triggers up to specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("--n-loudest", type = "int", help = "If set, only look at channels that produce the N loudest triggers in a given GPS time range.")
	parser.add_option("--cluster-cadence", type = "int", default = 1, help = "Sets the cadence at which trigger clustering is done. Default = 1s.")
	parser.add_option("-t", "--snr-threshold", type="float", default = 4., help = "Specify the minimum value of SNR that defines a glitch. Default glitch SNR = 4.")
	parser.add_option("-w", "--time-window", type="float", default = 1., help = "Specify the window of time in which an Omicron glitch and a gstlal glitch are the same. Default is 1s.")
	parser.add_option("--use-preloaded-triggers", action = "store_true", default = False, help = "If set, will assume triggers have already been processed before")
	parser.add_option("--cluster", help = "Set the cluster that this script is being run on (for proper public_html linking)")

	opts, args = parser.parse_args()
	
	# sanity checks
	if opts.start_time or opts.end_time:
		if not (opts.start_time and opts.end_time):
			parser.error("Need to specify both and a start and an end time.")

	# for verbose:
	if opts.verbose:
		print >> sys.stderr, "\nGetting to work...\n"
	
	return opts

def to_output_url(output_dir):
	username = os.getlogin()
	basepath = os.path.join(os.path.join('/home/', username), 'public_html')
	extension_url = os.path.relpath(os.path.abspath(output_dir), basepath)
	base_url = urlparse.urljoin(cluster_urls[opts.cluster], '~' + username)
	return base_url + '/' + extension_url

def generate_html_file(plot_paths, plot_strs):
	if opts.verbose:
		print >>sys.stderr, "Creating html report..."

	job_ids = set()
	#
	### head
	#
	title = "gstlal-Omicron comparison"
	metainfo = {'charset': 'utf-8', 'name': 'viewport', 'content': 'width=device-width, initial-scale=1'}
	doctype = '<!DOCTYPE html>'
	css = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	bootstrap = ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js']

	page = markup.page()
	page.init(title = title, metainfo = metainfo, doctype = doctype, css = css, script = bootstrap)

	#
	### body
	#
	page.div(class_ = 'container')

	# header
	page.h2('gstLAL-Omicron Comparison')
	page.hr()
	page.p('Run over 100000 seconds of MDC data, containing white noise burst and sine-gaussian injections')
	page.br()
	page.hr()

	# plots
	plot_paths = sorted(plot_paths, key=lambda x: x[0])
	for key in plot_paths:
		job_id, plot = key
		plot_url = to_output_url(opts.output_dir) + '/' + plot
		if job_id not in job_ids:
			job_ids.add(job_id)
			page.div(_class = 'col-md-12')
			page.h4('Results for channel %s' % key[0], align = 'center')
			for line in plot_strs[job_id].split('\n'):
				page.p(line)
			page.div.close()
		page.div(_class = 'col-md-6')
		page.div(_class = 'thumbnail')
		page.a(markup.oneliner.img(src = plot_url, alt = '', style = 'width:100%', _class='img-responsive'), href = plot_url, target = '_blank')
		page.div.close()
		page.div.close()	
	#
	### generate page	
	#
	page.div.close()
	with open(os.path.join(opts.output_dir, 'index.html'), 'w') as f:
		print >> f, page

	if opts.verbose:
		print >>sys.stderr, "done."

def determine_time_scale(tmin, tmax):
	time_int = tmax-tmin
	nticks = 6
	#FIXME: make this scaling more robust, this is kind of hackish
	# use hours
	if time_int > 6400.:
		hourtime = time_int/3600.
		stepsize = numpy.ceil(hourtime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*3600.)
		ticklabelrange = numpy.arange(0, hourtime, stepsize) 
		axlabel = 'hours'

	# use minutes
	elif time_int < 6400.:
		minutetime = time_int/60.
		stepsize = numpy.ceil(minutetime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*60.)
		ticklabelrange = numpy.arange(0, minutetime, stepsize)
		axlabel = 'minutes'	

	return tickrange, ticklabelrange, axlabel

def plot_missed_found(found_t, found_snr, missed_t, missed_snr, extra_t, extra_snr, ch_key):

	gps_start, gps_end = (opts.start_time, opts.end_time)
	
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	
	fig, axes = plt.subplots()
	
	axes.scatter(found_t, found_snr, color = 'k', alpha = 0.4, label = "Found")
	axes.scatter(missed_t, missed_snr, color = '#4C627D', alpha = 0.4, label = "Missed")
	axes.scatter(extra_t, extra_snr, marker='+', color='r', alpha = 0.4, label = "Extra gstlal triggers")
	
	axes.set_title(r"SNR vs time for Channel %s" % key)
	# FIXME: scale time correctly on the bottom
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_ylabel(r"SNR (Omicron)")
	axes.legend(loc='upper right', shadow=True)
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)
	
	return fig

def plot_snr_hist(paramstr, om_snrs, gst_snrs, ch_key, snr_cutoff):

	# settings
	n_bins = 50
	max_snr = max(max(gst_snrs), max(om_snrs))

	fig, axes = plt.subplots()

	axes.hist(om_snrs, numpy.linspace(snr_cutoff, max_snr, num=n_bins), histtype='bar', color=colors[0], alpha=0.5, normed=True, label = "Omicron")
	axes.hist(gst_snrs, numpy.linspace(snr_cutoff, max_snr, num=n_bins), histtype='bar', color=colors[1], alpha=0.5, normed=True, label = "GstLAL")

	title = axes.set_title("\n".join(wrap(r"%s for Channel %s" % (paramstr, key))))
	title.set_y(1.05)

	axes.set_title(r"%s for Channel %s" % (paramstr, key))
	axes.set_xlabel(r"SNR")
	axes.set_ylabel(r"Probability Density")
	axes.legend(loc='upper right', shadow=True)

	return fig

def plot_delta_t_hist(paramstr, times, time_window, ch_key, scale=None):

	# settings
	n_bins = 20

	fig, axes = plt.subplots()
	
	axes.hist(times, numpy.linspace(-0.5*time_window, 0.5*time_window, num=n_bins), histtype='bar', alpha=0.7, color=colors[2], normed=True)

	title = axes.set_title("\n".join(wrap(r"%s for Channel %s" % (paramstr, key))))
	title.set_y(1.05)
	
	axes.set_title(r"%s for Channel %s" % (paramstr, key))
	axes.set_xlabel(r"delta t")
	axes.set_ylabel(r"Probability Density")

	return fig

def plot_found_params(paramstr, om_time, om_params, gst_time, gst_params, ch_key, scale=None):

	gps_start, gps_end = (opts.start_time, opts.end_time)

	if om_params.size > 0 and gst_params.size > 0:
		# put a line on plot to indicate 1-1 correspondence 
		maxparam_val = max(max(om_params), max(gst_params))
		minparam_val = min(min(om_params), min(gst_params))

	else:
		maxparam_val = 1.
		minparam_val = 0.
		
	fig, axes = plt.subplots()
	
	axes.scatter(om_params, gst_params, color = colors[2], alpha = 0.4)

	# set scale if specified, otherwise linear
	if scale == 'loglog':
		axes.loglog()

	axes.plot(numpy.linspace(minparam_val,maxparam_val),numpy.linspace(minparam_val,maxparam_val), color = 'k')
	
	title = axes.set_title("\n".join(wrap(r"%s for Channel %s" % (paramstr, key))))
	title.set_y(1.05)
	axes.set_xlabel(r"Omicron %s" % paramstr)
	axes.set_ylabel(r"GstLAL %s" % paramstr)
	
	return fig

def plot_found_params_vs_time(paramstr, om_time, om_params, gst_time, gst_params, ch_key, scale=None):

	gps_start, gps_end = (opts.start_time, opts.end_time)
	
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	
	fig, axes = plt.subplots()

	if scale=='semi_logy':
		axes.semilogy()
	
	axes.scatter(om_time, om_params, color = '#4C627D', alpha = 0.4, label = "Omicron")
	axes.scatter(gst_time, gst_params, color = 'k', alpha = 0.4, label = "GstLAL")
	
	axes.set_title(r"%s vs time for Channel %s" % (paramstr, key))
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_ylabel(r"%s" % paramstr)
	axes.legend(loc='upper right', shadow=True)
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)
	
	return fig

def find_closest_values_indices(known_array, test_array):

	index_sorted = numpy.argsort(known_array)
	known_array_sorted = known_array[index_sorted]
	known_array_middles = known_array_sorted[1:] - numpy.diff(known_array_sorted.astype('f'))/2
	idx1 = numpy.searchsorted(known_array_middles, test_array)
	
	known_indices = index_sorted[idx1]
	test_indices = numpy.arange(len(known_indices))
	residual = test_array - known_array[known_indices]
	
	return known_indices, test_indices, residual


#
# =============================================================================
#
#                              Do it!
#
# =============================================================================
#

# create idq data object from file path
opts = parse_command_line()

#
# read in trigger data + preprocessing
#

# create instance of idq data class
if opts.verbose:
	print >> sys.stderr, "Reading trigger data from file (this will take a moment)..."

om_trgdata = triggers.OmicronTriggerData(opts.omicron_trigger_dir, start_time = opts.start_time, end_time = opts.end_time, file_format = opts.omicron_file_format, preloaded_triggers = opts.use_preloaded_triggers)
gst_trgdata = triggers.GstlalIdqTriggerData(opts.gstlal_trigger_dir, start_time = opts.start_time, end_time = opts.end_time, file_format = opts.gstlal_file_format, preloaded_triggers = opts.use_preloaded_triggers)

if opts.verbose:
	print >> sys.stderr, "...done.\n"

# check if same channels present in both gstlal and omicron data
if gst_trgdata.keys() != om_trgdata.keys():
	if opts.verbose:
		print >>sys.stderr, "Warning: Missing data for gstlal or omicron triggers for certain channels\n"
		print >>sys.stderr, "Removing data for channels which don't have a corresponding pair..."

	# find common (channel, rate) pair set to look at
	reduced_keys = set(gst_trgdata.keys()).intersection(om_trgdata.keys())

	if opts.verbose:
		print >>sys.stderr, "Total number of channels looked at = %d, with channels...\n" % len(reduced_keys)
		for channel in reduced_keys:
			print >>sys.stderr, channel

	if opts.verbose:
		print >> sys.stderr, "...done.\n"
else:
	reduced_keys = gst_trgdata.keys()


if opts.verbose:
	print >> sys.stderr, "Making an SNR cut to ingested triggers..."

# remove triggers that aren't above a specified SNR cutoff
gst_trgdata.filter_triggers(column = 'snr', threshold = opts.snr_threshold)
om_trgdata.filter_triggers(column = 'snr', threshold = opts.snr_threshold)

if opts.verbose:
	print >> sys.stderr, "...done.\n"

# if N loudest triggers are set, only look at channels that produce loud triggers
if opts.n_loudest:
	if opts.verbose:
		print >> sys.stderr, "Extracting %d loudest triggers for each pipeline..." % opts.n_loudest

	gst_n_loudest = gst_trgdata.sort_triggers()[:opts.n_loudest]
	om_n_loudest = om_trgdata.sort_triggers()[:opts.n_loudest]

	_, gst_loudest_keys = zip(*gst_n_loudest)
	_, om_loudest_keys = zip(*om_n_loudest)

	ch_keys = set(list(gst_loudest_keys)).union(list(om_loudest_keys))

	if opts.verbose:
		print >> sys.stderr, "...done.\n"
else:
	ch_keys = reduced_keys



#
# find trigger coincidences
#

if opts.verbose:
	print >> sys.stderr, "Finding trigger coincidences for all gstlal and omicron triggers...\n"

# window size in seconds, e.g. two glitches within this window represent the same glitch
time_window = opts.time_window

# find matches, missed and extra triggers
extra_om_triggers = {key: [] for key in ch_keys}
extra_gst_triggers = {key: [] for key in ch_keys}
om_gst_matches = {key: [] for key in ch_keys}
delta_t = {key: [] for key in ch_keys}

# create directory for report if one doesn't exist
if not os.path.exists(opts.output_dir):
	try:
		os.makedirs(opts.output_dir)
	except IOError:
		pass
	except OSError:
		pass

plot_strs = {}
with open(os.path.join(opts.output_dir, 'report.txt'), 'w') as f:

	f.write('Comparison report for gstlal vs omicron:\n\n')
	for key in sorted(ch_keys):

		# check if there are any triggers
		if len(gst_trgdata[key]['trigger_time']) == 0 or len(om_trgdata[key]['trigger_time']) == 0:
			out_str = '\tno matches found for channel = %s\n\n' % key
			f.write(out_str)
			print >>sys.stderr, out_str
			continue

		gst_times = gst_trgdata[key]['trigger_time']
		om_times = om_trgdata[key]['trigger_time']

		# check which set has more triggers to calculate time differences
		if len(gst_times) > len(om_times):
			gst_idxs, om_idxs, tdiff = find_closest_values_indices(gst_trgdata[key]['trigger_time'], om_trgdata[key]['trigger_time'])
		else:
			om_idxs, gst_idxs, tdiff = find_closest_values_indices(om_trgdata[key]['trigger_time'], gst_trgdata[key]['trigger_time'])

		match_filter = numpy.abs(tdiff) <= 0.5*time_window

		match_idxs = numpy.argwhere(match_filter)

		om_match_idxs = om_idxs[match_idxs]
		gst_match_idxs = gst_idxs[match_idxs]

		missed_om_filter = numpy.ones(len(om_trgdata[key]), dtype=bool)
		missed_gst_filter = numpy.ones(len(gst_trgdata[key]), dtype=bool)
		missed_om_filter[om_match_idxs] = False
		missed_gst_filter[gst_match_idxs] = False

		missed_om_idxs = numpy.argwhere(missed_om_filter)
		missed_gst_idxs = numpy.argwhere(missed_gst_filter)

		extra_om_triggers[key] = missed_om_idxs
		extra_gst_triggers[key] = missed_gst_idxs

		om_gst_matches[key] = list(zip(om_match_idxs, gst_match_idxs))

		delta_t[key] = tdiff[match_idxs]

		# find percentages of found/missed triggers
		if om_gst_matches[key]:
			num_found = len(match_idxs)
			num_missed = len(extra_om_triggers[key])
			num_extra = len(extra_gst_triggers[key])
			percent_found = 100. * num_found/(num_found+num_missed+num_extra)
			percent_extra_gst = 100. * num_extra/(num_found+num_missed+num_extra)
			percent_extra_omc = 100. * num_missed/(num_found+num_missed+num_extra)

			found_str = '\t%f percent matched triggers for channel =       %s\n' % (percent_found, key)
			extra_gst_str = '\t%f percent extra gstlal triggers for channel =  %s\n' % (percent_extra_gst, key)
			extra_omc_str = '\t%f percent extra omicron triggers for channel = %s\n\n' % (percent_extra_omc, key)

			out_str = ''.join([found_str, extra_gst_str, extra_omc_str])
			plot_strs[key] = out_str.replace('\t','')

		else:
			out_str = '\tno matches found for channel = %s\n\n' % key

		f.write(out_str)
		print >>sys.stderr, out_str

#
# write glitches in found, missed, extra sets to file in case they warrant further investigation
#

if opts.verbose:
	print >> sys.stderr, "...done.\n"
	print >> sys.stderr, "Writing summary of missed and found triggers to disk...\n"

plot_paths = []
for key in sorted(ch_keys):

	if om_gst_matches[key]:
		# make paths for output if they don't already exist
		#FIXME: clean up output
		outpath = os.path.join(opts.output_dir, key)
		if not os.path.exists(outpath):
			try:
				os.makedirs(outpath)
			except IOError:
				pass
			except OSError:
				pass
	
		# create output files
	
		# record trigger matches
		found_data = deque(maxlen = 25000)
		found_header = "# %14s\t%13s\t%13s\t%11s\t%15s\t%13s\t%14s\t%14s\n" % ("found_om_times", "found_om_snrs", "found_om_freq", "found_om_qs", "found_gst_times", "found_gst_snrs", "found_gst_freq", "found_gst_qs")
		found_data.append(found_header)
		for om_idx, gst_idx in om_gst_matches[key]:
			found_data.append("%20.9f\t%8.3f\t%8.3f\t%8.3f\t%20.9f\t%8.3f\t%8.3f\t%8.3f\n" % (om_trgdata[key][om_idx]['trigger_time'],
			                                                                                  om_trgdata[key][om_idx]['snr'],
			                                                                                  om_trgdata[key][om_idx]['frequency'],
			                                                                                  om_trgdata[key][om_idx]['q'],
			                                                                                  gst_trgdata[key][gst_idx]['trigger_time'],
			                                                                                  gst_trgdata[key][gst_idx]['snr'],
			                                                                                  gst_trgdata[key][gst_idx]['frequency'],
			                                                                                  gst_trgdata[key][gst_idx]['q']))
	
		if len(found_data) > 1:
			with open(os.path.join(outpath, "matched_glitch_data_%s_%s.txt" % (opts.time_window, opts.snr_threshold)), 'w') as f:
				f.write(''.join(found_data))
	
		# record missed triggers
		missed_data = deque(maxlen = 25000)
		missed_header = "# %15s\t%14s\t%14s\t%12s\n" % ("missed_om_times", "missed_om_snrs", "missed_om_freq", "missed_om_qs")
		missed_data.append(missed_header)
		for om_idx in extra_om_triggers[key]:
			missed_data.append("%20.9f\t%8.3f\t%8.3f\t%8.3f\n" % (om_trgdata[key][om_idx]['trigger_time'],
			                                                      om_trgdata[key][om_idx]['snr'],
			                                                      om_trgdata[key][om_idx]['frequency'],
			                                                      om_trgdata[key][om_idx]['q']))
	
		if len(missed_data) > 1:
			with open(os.path.join(outpath, "missed_glitch_data_%s_%s.txt" % (opts.time_window, opts.snr_threshold)), 'w') as f:
				f.write(''.join(missed_data))
	
		extra_gstdata = deque(maxlen = 25000)
		extra_gstheader = "# %15s\t%14s\t%14s\t%12s\n" % ("missed_gst_times", "missed_gst_snrs", "missed_gst_freq", "missed_gst_qs")
		extra_gstdata.append(extra_gstheader)
		for gst_idx in extra_gst_triggers[key]:
			extra_gstdata.append("%20.9f\t%8.3f\t%8.3f\t%8.3f\n" % (gst_trgdata[key][gst_idx]['trigger_time'],
			                                                        gst_trgdata[key][gst_idx]['snr'],
			                                                        gst_trgdata[key][gst_idx]['frequency'],
			                                                        gst_trgdata[key][gst_idx]['q']))
	
		if len(extra_gstdata) > 1:
			with open(os.path.join(outpath, "extra_gst_glitch_data_%s_%s.txt" % (opts.time_window, opts.snr_threshold)), 'w') as f:
				f.write(''.join(extra_gstdata))
	
		#
		# create plots
		#
	
		# indices for matched glitches
		print >> sys.stderr, "Creating plots for channel %s ...\n" % key

		om_match_idx, gst_match_idx = zip(*om_gst_matches[key])
		missed_om_idx = extra_om_triggers[key]
		extra_gst_idx = extra_gst_triggers[key]

		if len(om_match_idx) > 0:
			found_om = om_trgdata[key][numpy.array(om_match_idx)]
		if len(missed_om_idx) > 0:
			missed_om = om_trgdata[key][numpy.array(missed_om_idx)]
		if len(gst_match_idx) > 0:
			found_gst = gst_trgdata[key][numpy.array(gst_match_idx)]
		if len(extra_gst_idx) > 0:
			extra_gst = gst_trgdata[key][numpy.array(extra_gst_idx)]

		if om_match_idx and gst_match_idx:
			snr_hist_fig = plot_snr_hist("SNR histogram", found_om['snr'], found_gst['snr'], key, snr_cutoff = opts.snr_threshold)
			snr_hist_fig_fname = 'snr_hist.png'
			figpath = os.path.join(key, snr_hist_fig_fname)
			plot_paths.append((key,figpath))
			snr_hist_fig.savefig(os.path.join(outpath, snr_hist_fig_fname))
			plt.close(snr_hist_fig_fname)

			mf_fig = plot_missed_found(found_om['trigger_time'], found_om['snr'], missed_om['trigger_time'], missed_om['snr'], extra_gst['trigger_time'], extra_gst['snr'], key)
			mf_fig_fname = 'mf.png'
			figpath = os.path.join(key, mf_fig_fname)
			plot_paths.append((key,figpath))
			mf_fig.savefig(os.path.join(outpath, mf_fig_fname))
			plt.close(mf_fig)

			deltat_fig = plot_delta_t_hist("time difference", delta_t[key], time_window, key)
			deltat_fig_fname = 'delta_t_hist.png'
			figpath = os.path.join(key, deltat_fig_fname)
			plot_paths.append((key,figpath))
			deltat_fig.savefig(os.path.join(outpath, deltat_fig_fname))
			plt.close(deltat_fig_fname)

			snr_fig = plot_found_params("SNR", found_om['trigger_time'], found_om['snr'], found_gst['trigger_time'], found_gst['snr'], key, scale='loglog')
			snr_fig_fname = 'found_snrs.png'
			figpath = os.path.join(key, snr_fig_fname)
			plot_paths.append((key,figpath))
			snr_fig.savefig(os.path.join(outpath, snr_fig_fname))
			plt.close(snr_fig_fname)

			freq_fig = plot_found_params("peak frequency", found_om['trigger_time'], found_om['frequency'], found_gst['trigger_time'], found_gst['frequency'], key)
			freq_fig_fname = 'found_freqs.png'
			figpath = os.path.join(key, freq_fig_fname)
			plot_paths.append((key,figpath))
			freq_fig.savefig(os.path.join(outpath, freq_fig_fname))
			plt.close(freq_fig_fname)

			# phase comparison probably meaningless
			#phase_fig = plot_found_params("phase", found_om_times, found_om_phase, found_gst_times, found_gst_phase, key)
			#phase_fig_fname = 'found_phase_%s.png' % str(gps_range)
			#phase_fig.savefig(os.path.join(outpath, phase_fig_fname))
			#plt.close(phase_fig_fname)

			# q comparison meaningless since can't extract Q from omicron xml
			#q_fig = plot_found_params("Q number", found_om['trigger_time'], found_om['q'], found_gst['trigger_time'], found_gst['q'], key)
			#q_fig_fname = 'found_q.png'
			#q_fig.savefig(os.path.join(outpath, q_fig_fname))
			#plt.close(q_fig_fname)

			snrvstime = plot_found_params_vs_time("SNR", om_trgdata[key]['trigger_time'], om_trgdata[key]['snr'], gst_trgdata[key]['trigger_time'], gst_trgdata[key]['snr'], key, scale='semi_logy')
			snrvstime_fname = 'snrs_v_time.png'
			figpath = os.path.join(key, snrvstime_fname)
			plot_paths.append((key,figpath))
			snrvstime.savefig(os.path.join(outpath, snrvstime_fname))
			plt.close(snrvstime_fname)

			#snrvstime = plot_found_params_vs_time("SNR", found_om['trigger_time'], found_om['snr'], found_gst['trigger_time'], found_gst['snr'], key, scale='semi_logy')
			#snrvstime_fname = 'found_snrs_v_time.png'
			#snrvstime.savefig(os.path.join(outpath, snrvstime_fname))
			#plt.close(snrvstime_fname)

			freqvstime = plot_found_params_vs_time("peak frequency", om_trgdata[key]['trigger_time'], om_trgdata[key]['frequency'], gst_trgdata[key]['trigger_time'], gst_trgdata[key]['frequency'], key)
			freqvstime_fname = 'freq_v_time.png'
			figpath = os.path.join(key, freqvstime_fname)
			plot_paths.append((key,figpath))
			freqvstime.savefig(os.path.join(outpath, freqvstime_fname))
			plt.close(snrvstime_fname)

			#freqvstime = plot_found_params_vs_time("peak frequency", found_om['trigger_time'], found_om['frequency'], found_gst['trigger_time'], found_gst['frequency'], key)
			#freqvstime_fname = 'found_freq_v_time.png'
			#freqvstime.savefig(os.path.join(outpath, freqvstime_fname))
			#plt.close(freqvstime_fname)

			#phasevstime = plot_found_params_vs_time("phase", found_om['trigger_time'], found_om['phase'], found_gst['trigger_time'], found_gst['phase'], key)
			#phasevstime_fname = 'found_phase_v_time.png'
			#phasevstime.savefig(os.path.join(outpath, phasevstime_fname))
			#plt.close(phasevstime_fname)

	else:
		print >> sys.stderr, "No matches found for channel %s, skipping...\n" % key

# create results page
generate_html_file(plot_paths, plot_strs)
