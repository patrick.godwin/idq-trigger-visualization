# Copyright (C) 2017 Sydney J. Chamberlin, Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# =============================================================================
#
#                               PREAMBLE
#
# =============================================================================

import sys, os, glob
from collections import defaultdict
from collections import namedtuple
import datetime
import itertools
import math
import string

import h5py
import numpy

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import param
from glue.ligolw import utils as ligolw_utils

from etc import utils

# =============================================================================
#
#                               CLASSES
#
# =============================================================================

class GstlalIdqTriggerData(object):

	def __init__(self, triggerdir, start_time = None, end_time = None, preloaded_triggers = False, cluster_cadence = 1, file_format = 'hdf5', verbose = True):

		self.triggerdir = triggerdir
		self.start_time = start_time
		self.end_time = end_time
		self.cluster_cadence = cluster_cadence
		self.file_format = file_format
		self.preloaded_triggers = preloaded_triggers
		self.verbose = verbose

		# populate data structure with relevant triggers
		self.triggerdict = {}
		self._extract_triggers()

	def filter_triggers(self, column = 'snr', threshold = 8.0):
		"""
		filter triggers in place based on some threshold value and a column,
		default is snr with a threshold of 8.0
		"""
		for key in self.triggerdict.keys():
			filter_idx = numpy.where(self.triggerdict[key][column] >= threshold)
			self.triggerdict[key] = self.triggerdict[key][filter_idx]

	def sort_triggers(self, column = 'snr', descending = True):
		"""
		returns a sorted list of descending triggers based on column,
		in the form of (row, channel)
		"""
		rows = [(row, channel) for channel, value in self.triggerdict.items() if value.shape[0] > 0 for row in numpy.split(value, value.shape[0], axis=0)]
		return [row for row in sorted(rows, key=lambda e: e[0][column][0], reverse=descending)] 

	def cluster_triggers(self, data, aggregate = 'max', column = 'snr', axis=0):
		"""
		helper method to cluster triggers based on some aggregate and a column, e.g. max(snr)

		returns a trigger subset based on clustering
		"""
		# remove all nan rows before clustering
		nonnan_indices = numpy.where(numpy.isfinite(data['snr']))
		data = data[nonnan_indices]
		# find indices of aggregate result
		if data.size > 0:
			if aggregate == 'max':
				idx = numpy.argmax(data[column], axis=axis)
			else:
				raise NotImplementedError

			return data[idx]
		else:
			return None

	def _extract_triggers(self):
		"""
		helper method to extract triggers from disk given some file format, default is 'ascii', but also accepts 'hdf5'
		"""

		if self.file_format == 'hdf5':
			# grab relevant trigger files for each gps range
			interval = self.end_time - self.start_time
			gps_idx = len(str(self.end_time)) - len(str(interval))

			glob_exp = '*/*/*/*'
			for idx in range(len(str(self.end_time))):
				if idx == gps_idx:
					effective_start_time = self.start_time
					glob_exp += '[' + str(effective_start_time)[idx] + '-' + str(self.end_time - 1)[idx] + ']'
				else:
					glob_exp += '[0-9]'
			glob_exp += '*.h5'

			# open hdf file and find datasets corresponding to a specific channel and rate
			save_cadence = None
			row_dtypes = None

			# get all files by gps range
			hdf_paths = sorted(glob.glob(os.path.join(self.triggerdir, glob_exp)))

			# start reading through each file
			for hdf_path in hdf_paths:

				if self.verbose:
					print >>sys.stderr, "Reading h5 file from: %s" % hdf_path

				# find out which hdf groups to get from file
				with h5py.File(hdf_path, 'r') as hfile:
					channels = hfile.keys()

					for channel in channels:
						if self.verbose:
							print >>sys.stderr, "Processing triggers from channel: %s" % channel

						if self.preloaded_triggers:
							dataset = hfile[channel].keys()[0]
							self.triggerdict[channel] = numpy.array(hfile[channel][dataset])
						else:
							# get all rates and datasets for a given channel
							# assumes that all rates for a given channel have the same datasets associated with them
							rates = hfile[channel].keys()
							datasets = hfile[channel][rates[0]].keys()

							if datasets:
								# grab relevant datasets based on gps range
								# FIXME: think about start time here
								relevant_datasets = [dataset for dataset in datasets if self.start_time <= int(dataset.split('_')[0]) <= self.end_time]

								# grab save cadence and numpy dtypes for datasets
								# FIXME: should be using hdf metadata for this instead
								if not (save_cadence and row_dtypes):
									save_cadence = int(datasets[0].split('_')[1])
									row_dtypes = hfile[os.path.join(channel, rates[0], relevant_datasets[0])].dtype

								# preallocate space and initialize all values to nan
								rows = numpy.empty((save_cadence * len(relevant_datasets),), dtype=row_dtypes)
								rows[:] = numpy.nan

								# loop through each dataset
								for idx, dataset in enumerate(relevant_datasets):
									if self.verbose:
										print >>sys.stderr, "Processing triggers from dataset: %s" % dataset

									hdf_keys = [os.path.join(channel, rate, dataset) for rate in rates]

									# combine datasets for all rates
									triggers = numpy.array([numpy.array(hfile[key]) for key in hdf_keys if key in hfile])

									# grab max snr indices for rates
									idxmax = numpy.argmax(triggers['snr'], axis=0)

									# get row for each timestamp corresponding with max snr for rates
									for idx_row, idx_max in enumerate(idxmax):
										idx_start = idx * save_cadence
										max_row = triggers[idx_max][idx_row]

										rows[idx_start + idx_row] = max_row

								# append to trigger dict
								if channel in self.triggerdict:
								        self.triggerdict[channel] = numpy.concatenate((self.triggerdict[channel], rows), axis=0)
								else:
								        self.triggerdict[channel] = rows

			for channel in self.triggerdict.keys():
				# remove all rows with nan and convert to numpy record array
				nonnan_indices = numpy.where(numpy.isfinite(self.triggerdict[channel]['snr']))
				self.triggerdict[channel] = self.triggerdict[channel][nonnan_indices].view(numpy.recarray)

		else:
			raise NotImplementedError

	def items(self):
		return self.triggerdict.items()

	def keys(self):
		return self.triggerdict.keys()

	def values(self):
		return self.triggerdict.values()

	def __len__(self):
		return len(self.triggerdict)

	def __getitem__(self, key):
		return self.triggerdict[key]

	def __contains__(self, key):
		return key in self.triggerdict

	def __repr__(self):
		return repr(self.triggerdict)

class OmicronTriggerData(object):

	def __init__(self, triggerdir, start_time = None, end_time = None, preloaded_triggers = False, cluster_cadence = 1, file_format = 'xml', verbose = True, channel = None):
		"""
		Returns object with various omicron trigger parameters.
		"""
		self.triggerdir = triggerdir
		#FIXME: make sure channel name has same format for gstlal and omicron triggers
		self.start_time = start_time
		self.end_time = end_time
		self.cluster_cadence = cluster_cadence
		self.file_format = file_format
		self.preloaded_triggers = preloaded_triggers
		self.verbose = verbose
		self.channel = channel

		# populate data structure with relevant triggers
		self.triggerdict = {}
		self._extract_triggers()
	
	def filter_triggers(self, column = 'snr', threshold = 8.0):
		"""
		filter triggers in place based on some threshold value and a column,
		default is snr with a threshold of 8.0
		"""
		for key in self.triggerdict.keys():
			filter_idx = numpy.where(self.triggerdict[key][column] >= threshold)
			self.triggerdict[key] = self.triggerdict[key][filter_idx]

	def sort_triggers(self, column = 'snr', descending = True):
		"""
		returns a sorted list of descending triggers based on column,
		in the form of (row, key) where key = (gps_range, channel, rate)
		"""
		rows = [(row, key) for key, value in self.triggerdict.items() if value.shape[0] > 0 for row in numpy.split(value, value.shape[0], axis=0)]
		return [row for row in sorted(rows, key=lambda e: e[0][column][0], reverse=descending)] 

	def cluster_triggers(self, data, aggregate = 'max', column = 'snr'):
		"""
		helper method to cluster triggers based on some aggregate and a column, e.g. max(snr)

		returns a trigger subset based on clustering
		"""
		# remove all nan rows before clustering
		nonnan_indices = numpy.where(numpy.isfinite(data['snr']))
		data = data[nonnan_indices]

		# find indices of aggregate result
		if aggregate == 'max':
			idx = numpy.argmax(data[column])
		else:
			raise NotImplementedError

		return data[idx]

	def _extract_triggers(self):
		"""
		helper method to extract triggers from disk given omicron trigger files, either ascii, xml or previously saved triggers in hdf5
		"""
		# Define named structures for clarity
		Row = namedtuple('Row', ['trigger_time', 'snr', 'frequency', 'phase', 'q'])
		row_dtype = [(column, 'f8') for column in Row._fields]
	
		# structure to store unclustered triggers (if clustering)
		triggers = {}

		if self.file_format == 'xml':

			# define a content handler
			class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
			    pass

			lsctables.use_in(LIGOLWContentHandler)

			# grab relevant trigger files for each gps range
			interval = self.end_time - self.start_time
			gps_idx = len(str(self.end_time)) - len(str(interval))

			# if a channel is specified, grab only those files instead
			if self.channel:
				# production triggers
				#ifo, channel = self.channel.split(':')
				#channel = channel.replace('-', '_')+'_OMICRON'
				#glob_exp = '%s/*/*' % channel
				# MDC triggers
				glob_exp = '%s/*' % self.channel
				# custom triggers
				#glob_exp = '%s/*/*' % self.channel
				#glob_exp = '[0-9]*/*/%s/*' % self.channel
			else:
				# production triggers
				glob_exp = '*/*/*'
				# MDC triggers
				glob_exp = '*/*'
				#glob_exp = '[0-9]*/*/*/*'

			for idx in range(len(str(self.end_time))):
				if idx == gps_idx:
					effective_start_time = self.start_time
					glob_exp += '[' + str(effective_start_time)[idx] + '-' + str(self.end_time - 1)[idx] + ']'
				else:
					glob_exp += '[0-9]'
			#glob_exp += '*.xml'
			# production triggers
			#glob_exp += '*.xml.gz'
			# MDC triggers
			glob_exp += '*.new.xml.gz'
			print >>sys.stderr,  os.path.join(self.triggerdir, glob_exp)

			# extract trigger data and match column headings to data
			trgfiles = sorted(glob.iglob(os.path.join(self.triggerdir, glob_exp)))

			# construct trigger dictionary
			for trgfile in trgfiles:

				# load xml doc
				xmldoc = ligolw_utils.load_filename(trgfile, contenthandler = LIGOLWContentHandler, verbose = self.verbose)

				# get tabular format (sngl burst for omicron)
				sngl_burst_table = lsctables.SnglBurstTable.get_table(xmldoc)

				if self.channel:
					channel = self.channel
				else:
					process_params_table = lsctables.ProcessParamsTable.get_table(xmldoc)

					# get channel name and rate
					channel = [row.param for row in process_params_table if row.value == 'omicron_DATA_CHANNEL'][0]

				if channel not in triggers:
					triggers[channel] = []

					# preallocate space and initialize all values to nan
					self.triggerdict[channel] = numpy.empty((interval,), dtype=row_dtype)
					self.triggerdict[channel][:] = numpy.nan

				for row in sngl_burst_table:
					# get central time of trigger
					trg_time = row.peak_time + row.peak_time_ns * 1e-9

					# filter out rows that don't span the gps range
					if trg_time >= int(self.start_time) and trg_time <= int(self.end_time) + 1:
						# production triggers
						#trigger_row = Row(trigger_time = trg_time, snr = row.snr, frequency = row.central_freq, phase = row.param_one_value, q = 0)
						# MDC triggers
						trigger_row = Row(trigger_time = trg_time, snr = row.snr, frequency = row.central_freq, phase = 0, q = 0)

						# check if trigger is inside time window specified by cadence, if not, cluster first
						if triggers[channel] and utils.in_new_epoch(trigger_row.trigger_time, triggers[channel][0].trigger_time, self.cluster_cadence):
							data = self.cluster_triggers(numpy.array(triggers[channel], dtype = row_dtype))

							trg_idx = numpy.floor(data['trigger_time']) % interval
							# check if there's an already existing trigger in this gps range
							# if there isn't, store trigger
							if numpy.isnan(self.triggerdict[channel][trg_idx]['snr']):
								self.triggerdict[channel][trg_idx] = data
							# if there is, update with new trigger if new snr is higher
							elif data['snr'] > self.triggerdict[channel][trg_idx]['snr']:
								self.triggerdict[channel][trg_idx] = data

							triggers[channel][:] = []

						# add unclustered trigger
						triggers[channel].append(trigger_row)

			# cluster remaining triggers together for each gps range
			for channel in self.triggerdict.keys():
				if triggers[channel]:
					data = self.cluster_triggers(numpy.array(triggers[channel], dtype = row_dtype))

					trg_idx = numpy.floor(data['trigger_time']) % interval
					# check if there's an already existing trigger in this gps range
					# if there isn't, store trigger
					if numpy.isnan(self.triggerdict[channel][trg_idx]['snr']):
						self.triggerdict[channel][trg_idx] = data
					# if there is, update with new trigger if new snr is higher
					elif data['snr'] > self.triggerdict[channel][trg_idx]['snr']:
						self.triggerdict[channel][trg_idx] = data

					triggers[channel][:] = []

			# remove all rows with nan and convert to numpy record array
			for channel in self.triggerdict.keys():
				nonnan_indices = numpy.where(numpy.isfinite(self.triggerdict[channel]['snr']))
				self.triggerdict[channel] = self.triggerdict[channel][nonnan_indices].view(numpy.recarray)

		elif self.file_format == 'hdf5':
			interval = self.end_time - self.start_time
			gps_idx = len(str(self.end_time)) - len(str(interval))

			glob_exp = '*/*/*/*'
			for idx in range(len(str(self.end_time))):
				if idx == gps_idx:
					effective_start_time = self.start_time
					glob_exp += '[' + str(effective_start_time)[idx] + '-' + str(self.end_time - 1)[idx] + ']'
				else:
					glob_exp += '[0-9]'
			glob_exp += '*.h5'

			# get all files by gps range
			hdf_paths = glob.glob(os.path.join(self.triggerdir, glob_exp))

			# start reading through each file
			for hdf_path in hdf_paths:

				# open hdf file and find datasets corresponding to a specific channels
				with h5py.File(hdf_path, 'r') as hfile:
					# find out channels to get from file
					channels = hfile.keys()

					save_cadence = None
					row_dtypes = None

					for channel in channels:
						if self.verbose:
							print >>sys.stderr, "Processing triggers from channel: %s" % channel

						# find datasets corresponding to each group
						datasets = hfile[channel].keys()

						if datasets:
							# grab save cadence and numpy dtypes for datasets
							# FIXME: should be using hdf metadata for this instead
							if not (save_cadence and row_dtypes):
								save_cadence = int(datasets[0].split('_')[1])
								row_dtypes = hfile[channel][datasets[0]].dtype

							# grab relevant datasets based on gps range
							relevant_datasets = [dataset for dataset in datasets if self.start_time <= int(dataset.split('_')[0]) <= self.end_time]

							if self.preloaded_triggers:
								self.triggerdict[channel] = numpy.array(hfile[channel][dataset])
							else:
								raise NotImplementedError

	def items(self):
		return self.triggerdict.items()

	def keys(self):
		return self.triggerdict.keys()

	def values(self):
		return self.triggerdict.values()

	def __len__(self):
		return len(self.triggerdict)

	def __getitem__(self, key):
		return self.triggerdict[key]

	def __contains__(self, key):
		return key in self.triggerdict

	def __repr__(self):
		return repr(self.triggerdict)
