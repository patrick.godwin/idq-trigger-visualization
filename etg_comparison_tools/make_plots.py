#!/usr/bin/env python

import optparse
import datetime
import itertools
import math
import os
import sys

import numpy
import h5py

from textwrap import wrap
from decimal import Decimal
from lal import GPSToUTC

from etc import triggers

import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.colorbar import Colorbar
from matplotlib import pyplot as plt
from matplotlib import ticker as tkr
import matplotlib.cm as cm

matplotlib.rcParams.update({
        "font.size": 10.0,
        "axes.titlesize": 10.0,
        "axes.labelsize": 10.0,
        "xtick.labelsize": 8.0,
        "ytick.labelsize": 8.0,
        "legend.fontsize": 8.0,
        "figure.dpi": 200,
        "savefig.dpi": 200
})

def parse_command_line():
	"""
	Parse command line inputs.
	"""
	parser = optparse.OptionParser()
	parser.add_option("-m", "--trigger-dir", default = ".", help = "Specify directory containing trigger files. All triggers will be plotted unless GPS time range is specified with --start-time and --stop-time options.")
	parser.add_option("-o", "--output-dir", action = "store", default = ".", help = "Specify location for output files. Default is current directory.")	
	parser.add_option("-s", "--start-time", type = "int", help = "Plot data for triggers starting at specified GPS time. Specified time must agree with times in trigger files.") 
	parser.add_option("-e", "--end-time", type = "int", help = "Plot data for triggers up to specified GPS time. Specified time must agree with times in trigger files.") 

	opts, args = parser.parse_args()
	
	# sanity checks
	if opts.start_time or opts.end_time:
		if not (opts.start_time and opts.end_time):
			parser.error("Need to specify both and a start and an end time.")

	return opts

def determine_time_scale(tmin, tmax):
	time_int = tmax-tmin
	nticks = 6
	#FIXME: make this scaling more robust, this is kind of hackish
	# use hours
	if time_int > 6400.:
		hourtime = time_int/3600.
		stepsize = numpy.ceil(hourtime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*3600.)
		ticklabelrange = numpy.arange(0, hourtime, stepsize) 
		axlabel = 'hours'

	# use minutes
	elif time_int < 6400.:
		minutetime = time_int/60.
		stepsize = numpy.ceil(minutetime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*60.)
		ticklabelrange = numpy.arange(0, minutetime, stepsize)
		axlabel = 'minutes'	

	return tickrange, ticklabelrange, axlabel

def plot_found_params_vs_time(paramstr, time, params, key, scale=None):

	gps_start, gps_end = (opts.start_time, opts.end_time)
	
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	
	fig, axes = plt.subplots()

	if scale=='semi_logy':
		axes.semilogy()
	
	axes.scatter(time, params, color = 'k', alpha = 0.4, label = "GstLAL")
	
	axes.set_title(r"%s vs time for Channel %s" % (paramstr, key))
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_ylabel(r"%s" % paramstr)
	axes.legend(loc='upper left', shadow=True)
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)
	
	return fig

if __name__ == "__main__":

	opts = parse_command_line()

	trgdata = triggers.GstlalIdqTriggerData(opts.trigger_dir, start_time = opts.start_time, end_time = opts.end_time, file_format = 'hdf5')
	channel = trgdata.keys()[0]

	snrvstime = plot_found_params_vs_time("SNR", trgdata[channel]['trigger_time'], trgdata[channel]['snr'], channel, scale='semi_logy')
	snrvstime_fname = 'snrs_v_time.png'
	snrvstime.savefig(os.path.join(opts.output_dir, snrvstime_fname))
	plt.close(snrvstime_fname)

