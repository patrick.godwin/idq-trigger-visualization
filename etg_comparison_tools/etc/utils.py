__description__ = "a python module with some basic utilities"
__author__ = "Sydney J. Chamberlin (sydney.chamberlin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import numpy

#-------------------------------------------------

def find_glitches(trigger_tuple_list, snr_threshold):
	"""Takes in trigger parameter tuple and returns those that meet criteria to be a glitch."""
	glitch_tuples = []
        for tpl in trigger_tuple_list:
                if tpl[1] >= snr_threshold:
                        glitch_tuples.append( tpl )
        return glitch_tuples

def cluster(trigger_parameters):
        """Returns peak value from given list of parameter values."""
        clustered_parameter = max(trigger_parameters)
        return clustered_parameter

def cluster_data(glitch_dict):
	"""Clusters glitches within each integer GPS second, returning peak values from clustered glitch parameters."""
        int_times = []
        for idx, time in enumerate(glitch_dict.keys()):
                int_times.append(str(time).split('.')[0])

        # get rid of duplicates
        int_times = set(int_times)

        grouped_by_int_time = []
        for inttm in int_times:
                match_times = []
                match_params = []
                for time in glitch_dict.keys():
                        if time.split('.')[0] == inttm:
                                match_times.append(time)
                                match_params.append(glitch_dict[time])

                grouped_by_int_time.append((match_times,match_params))

        #for tmlist, paramlist in grouped_by_int_time:
        clustered_data = []
        for glist in grouped_by_int_time:
                ave_time = numpy.mean([float(i) for i in glist[0]])
                snrs, freqs, phases, qs = zip(*glist[1])
                clustered_snrs = cluster(snrs)
                clustered_freqs = cluster(freqs)
                clustered_phases = cluster(phases)
                clustered_qs = cluster(qs)
                clustered_data.append((ave_time, clustered_snrs, clustered_freqs, clustered_phases, clustered_qs))

        return clustered_data

def flatten_list(listoflists):
	"""Flattens a list of nested lists."""
        flattened = []
        for sublist in listoflists:
                for item in sublist:
                        flattened.append(item)

        return flattened

def contained_idxs(start_time, end_time, timelist):
	"""Given a list of times and start/end time, returns the indices for times that lie within a GPS range."""
        in_time_idxs = []
        for idx, time in enumerate(timelist):
                if float(time) >= start_time and float(time) <= end_time:
                        in_time_idxs.append(idx)

        return in_time_idxs

def in_new_epoch(new_gps_time, prev_gps_time, gps_epoch):
	"""
	Returns whether new and old gps times are in different
	epochs.
	"""
	return (new_gps_time - floor_div(prev_gps_time, gps_epoch)) >= gps_epoch

def floor_div(x, n):
	"""
	Floor an integer by removing its remainder
	from integer division by another integer n.
	e.g. floor_div(163, 10) = 160
	e.g. floor_div(158, 10) = 150
	"""
	assert n > 0
	return (x / n) * n
